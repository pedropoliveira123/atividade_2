//Crie um algoritmo que solicite o nome de um usuário e exiba uma mensagem de boas-vindas personalizada de acordo com o horário do dia (bom dia, boa tarde ou boa noite).
namespace exercicio_5
{
    const nome ="Pedro";
    const data = new Date();
    const horas = data.getHours();

    if (horas>=5 && horas<=12)
    {
        console.log(`Bom dia ${nome}`);
    }
    else if (horas>12 && horas<=18)
    {
        console.log(`Boa tarde ${nome}`);
    }
    else 
    {
        console.log(`Boa noite ${nome}`);
        
    }
}