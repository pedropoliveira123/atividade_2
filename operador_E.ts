//Testando o operador E - &&
namespace operador_E
{
    let idade = 17;
    let maiorIdade = idade > 18;
    let possuiCarteiraDeMotorista = true;

    let podeDirigir = maiorIdade && possuiCarteiraDeMotorista;

    console.log(podeDirigir); //true
}