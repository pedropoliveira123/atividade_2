//A nota final de um estudante é calculada a partir de três notas atribuídas respectivamente a um trabalho de laboratório, a uma avaliação semestral e a um exame final. A média das três notas mencionadas anteriormente obedece aos pesos a seguir. Faça um programa que receba as três notas, calcule e mostre a média ponderada e o conceito que segue a tabela abaixo:
namespace exercicio_2
{
    let trabLab, peso1, avSemestral, peso2, exFinal, peso3: number;

    trabLab = 8;
    peso1 = 2;
    avSemestral = 4;
    peso2 = 3;
    exFinal = 3;
    peso3 = 5;
    
    let media:number;

    media = (trabLab * peso1 + avSemestral * peso2 + exFinal * peso3) /(peso1 + peso2 +peso3);

    console.log(`A média ponderada é ${media}`);
    

    if (media>=8 && media<10)
    {
        console.log("Conceito A")
    }
    if (media>=7 && media<8)
    {
        console.log("Conceito B")
        
    }
    if (media>=6 && media<7)
    {
        console.log("Conceito C")
        
    }
    if (media>=5 && media<6)
    {
        console.log("Conceito D")
        
    }
    if (media>=0 && media<5)
    {
        console.log("Conceito E")
    }
   
    
}